package com.muhfai.productservice.controller;

import com.muhfai.productservice.dto.ProductResponse;
import com.muhfai.productservice.model.Product;
import com.muhfai.productservice.service.ProductService;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.math.BigDecimal;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api/product")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;

    @PostMapping("/import-data")
    public ProductResponse importData(@RequestParam("file") MultipartFile multipartFile){
        ProductResponse productResponse = new ProductResponse();

        List<Product> productList = new ArrayList<>();
        String line = "";
        try{
            Reader reader = new InputStreamReader(multipartFile.getInputStream());
            BufferedReader br = new BufferedReader(reader);
            while((line = br.readLine())!=null){
                Product product = new Product();
                String[] productLine = line.split(Pattern.quote("|"));
                product.setName(productLine[0]);
                Date newDate = new SimpleDateFormat("yyyy-MM-dd").parse(productLine[1]);
                product.setDob(newDate);
                product.setPaymentAmount(new BigDecimal(productLine[2]));
                product.setProductName(productLine[3]);
                productList.add(product);
            }
            productService.importData(productList);
            productResponse.setData(productList);
            productResponse.setIsError(false);
            productResponse.setMessage("Import Success!");
        }catch (Exception e){
            productResponse.setData(productList);
            productResponse.setIsError(false);
            productResponse.setMessage("Error Import File : " +  e.getMessage());
            e.printStackTrace();
        }
        return productResponse;
    }

    @GetMapping("/readCustomer")
    public List<Product> findAllCustomer(){
        return productService.findAllCustomer();
    }
}
