package com.muhfai.productservice.service;

import com.muhfai.productservice.model.Product;
import com.muhfai.productservice.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductRepository productRepository;

    public void importData(List<Product> productList){
        productRepository.saveAll(productList);
    }

    public List<Product> findAllCustomer(){
        return  productRepository.findAll();
    }
}
