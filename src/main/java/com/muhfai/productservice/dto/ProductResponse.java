package com.muhfai.productservice.dto;

import com.muhfai.productservice.model.Product;
import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class ProductResponse {
    private String message;
    private Boolean isError;
    private List<Product> data;
}
